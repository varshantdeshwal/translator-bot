import api_key from "../common/key";
var result = async function translate(text, language) {
  const res=await fetch('https://translate.yandex.net/api/v1.5/tr.json/translate?key='
  + api_key
  + '&lang='
  + encodeURIComponent(language)
  + '&text='
  + encodeURIComponent(text));

  // const res = await fetch(
  //   "https://translation.googleapis.com/language/translate/v2?target=" +
  //     language +
  //     "&key=" +
  //     api_key +
  //     "&q=" +
  //     text,

  //   {
  //     method: "POST"
  //   }
  // );
  const data = await res.json();
console.log(data);
  return data;
};
export default result;
