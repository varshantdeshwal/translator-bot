import React, { Component } from "react";
import "./App.css";
import Editor from "./components/code-editor/Editor";
import ChatBot from "./components/chat-bot/Main";

class App extends Component {
  render() {
    return (
      <div className="panels">
        <Editor />
        <ChatBot />
      </div>
    );
  }
}

export default App;
