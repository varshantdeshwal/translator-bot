import React from "react";

const UserChatMessage = props => {
  return (
    <div className="user-chat-message-container">
      <i className="fas fa-user user-icon " />
      <div className="chat-message ">{props.message}</div>
    </div>
  );
};
export default UserChatMessage;
