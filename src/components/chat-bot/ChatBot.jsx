import React, { Component } from "react";
import Chat from "./Chat";
import UserMessage from "./UserMessage";
const ChatBot = props => {
  return (
    <div className="chat-bot">
      <Chat />
      <UserMessage />
    </div>
  );
};

export default ChatBot;
