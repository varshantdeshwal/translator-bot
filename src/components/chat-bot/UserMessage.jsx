import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { onUserMessageChange, onEvaluation } from "../../actions/index";
import translate from "../../api-calls/translate";
var safeEval = require("safe-eval");
class UserMessage extends Component {
  evaluate = e => {
    e.preventDefault();
    this.props.onEvaluation({ type: "user", message: this.props.userMessage });
    this.props.onEvaluation({ type: "bot", message: "..." });
    if (this.props.applyChanges) {
      let code =
        "(" + this.props.codeToEvaluate + ')("' + this.props.userMessage + '")';

      var context = {
        translateFunction: (text, lang) =>
          translate(text, lang).then(res => {
            return res.text;
            // return res.data.translations[0].translatedText;
          })
      };

      var res = safeEval(code, context);
      if (res) {
        res.then(result => {
          this.props.onEvaluation({
            type: "bot",
            message: result
          });
        });
      } else {
        this.props.onEvaluation({
          type: "bot",
          message: this.props.userMessage
        });
      }
    } else {
      this.props.onEvaluation({ type: "bot", message: this.props.userMessage });
    }
  };
  render() {
    return (
      <div className="user-message">
        <form onSubmit={this.evaluate}>
          <input
            type="text"
            placeholder="Type message here"
            className="user-message-box"
            onChange={e => this.props.onUserMessageChange(e.target.value)}
            value={this.props.userMessage}
            required
          />
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userMessage: state.userMessage,
    codeToEvaluate: state.editorCode[0],
    applyChanges: state.applyChanges
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { onUserMessageChange: onUserMessageChange, onEvaluation: onEvaluation },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserMessage);
