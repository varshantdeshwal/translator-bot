import React from "react";

const BotChatMessage = props => {
  return (
    <div className="bot-chat-message-container">
      <i className="fas fa-robot bot-icon" />
      <div className="chat-message">{props.message}</div>
    </div>
  );
};
export default BotChatMessage;
