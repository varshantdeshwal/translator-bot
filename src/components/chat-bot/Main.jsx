import React, { Component } from "react";
import ChatBot from "./ChatBot";
import { onApplyChangesToggle } from "../../actions/index";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
class Main extends Component {
  toggleApplyChanges = () => {
    this.props.onApplyChangesToggle(!this.props.applyChanges);
  };
  render() {
    return (
      <div className="chat-bot-container">
        <div className="apply-change-button">
          <input
            type="button"
            value="apply changes"
            onClick={this.toggleApplyChanges}
            className={
              this.props.applyChanges
                ? "apply-changes-true"
                : "apply-changes-false"
            }
          />
        </div>
        <ChatBot />
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    applyChanges: state.applyChanges
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      onApplyChangesToggle: onApplyChangesToggle
    },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
