import React, { Component } from "react";

import { connect } from "react-redux";
import UserChatMessage from "./UserChatMessage";
import BotChatMessage from "./BotChatMessage";
class Chat extends Component {
  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }
  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  };
  render() {
    return (
      <div className="chat">
        {this.props.chat.map(msg => {
          if (msg.type === "user")
            return <UserChatMessage message={msg.message} />;
          return <BotChatMessage message={msg.message} />;
        })}
        <div
          style={{ float: "left", clear: "both" }}
          ref={element => {
            this.messagesEnd = element;
          }}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    chat: state.chat
  };
}

export default connect(
  mapStateToProps,
  null
)(Chat);
