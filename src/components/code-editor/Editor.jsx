import React, { Component } from "react";
import MonacoEditor from "react-monaco-editor";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  onEditorCodeChange,
  onAddingEditor,
  onActiveTabChanged,
  onClosingEditor
} from "../../actions/index";

import { TabContainer, TabPane, NavItem, Nav } from "react-bootstrap";

class Editor extends Component {
  editorDidMount(editor, monaco) {
    console.log("editorDidMount", editor);
    editor.focus();
  }
  handleSelect = key => {
    this.props.onActiveTabChanged(key);
  };
  closeEditor = key => {
    if (key <= this.props.activeTab) {
      this.props.onActiveTabChanged(this.props.activeTab - 1);
    }
    this.props.onClosingEditor(key);
  };
  addEditor = e => {
    this.props.onAddingEditor();
    this.props.onActiveTabChanged(this.props.code.length);
  };
  render() {
    const options = {
      selectOnLineNumbers: true
    };

    let allTabs = [...this.props.code];
    allTabs.shift();

    return (
      <TabContainer
        id="tabs-with-dropdown"
        defaultActiveKey={0}
        className="tab-container"
        onSelect={this.handleSelect}
        activeKey={this.props.activeTab}
      >
        <div className="tabs-display">
          <div>
            <Nav bsStyle="tabs  tab-name-panel">
              <NavItem eventKey={0} style={{ textDecoration: "none" }}>
                Main
              </NavItem>

              {allTabs.map((code, index) => {
                return (
                  <NavItem
                    eventKey={index + 1}
                    className="tab-name"
                    style={{ textDecoration: "none" }}
                  >
                    <span className="tab-text">Tab {index + 1}</span>
                    <i
                      class="fas fa-times close-icon"
                      onClick={e => {
                        e.stopPropagation();
                        this.closeEditor(index + 1);
                      }}
                    />
                  </NavItem>
                );
              })}

              <i class="fas fa-plus add-icon " onClick={this.addEditor} />
            </Nav>
          </div>

          {/* <TabContent animation={false} className="tab-content"> */}

          <div className="tab-pane">
            <TabPane eventKey={this.props.activeTab}>
              <div style={{ height: "100%" }}>
                <MonacoEditor
                  width="100%"
                  height="100%"
                  language="javascript"
                  theme="vs-dark"
                  value={this.props.code[this.props.activeTab]}
                  options={options}
                  onChange={(newValue, e) =>
                    this.props.onEditorCodeChange(
                      newValue,
                      this.props.activeTab
                    )
                  }
                />
              </div>
            </TabPane>
          </div>
          {/* </TabContent> */}
        </div>
      </TabContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    code: state.editorCode,
    applyChanges: state.applyChanges,
    activeTab: state.activeTab
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      onEditorCodeChange: onEditorCodeChange,
      onAddingEditor: onAddingEditor,
      onActiveTabChanged: onActiveTabChanged,
      onClosingEditor: onClosingEditor
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Editor);
