export const onEditorCodeChange = (code, key) => {
  return {
    type: "ON_EDITOR_CODE_CHANGE",
    code,
    key
  };
};
export const onAddingEditor = () => {
  return {
    type: "ON_ADDING_EDITOR"
  };
};
export const onClosingEditor = key => {
  return {
    type: "ON_CLOSING_EDITOR",
    key
  };
};
export const onActiveTabChanged = key => {
  return {
    type: "ON_ACTIVE_TAB_CHANGED",
    key
  };
};

export const onUserMessageChange = userMessage => {
  return {
    type: "ON_USER_MESSAGE_CHANGE",
    userMessage
  };
};

export const onApplyChangesToggle = applyChanges => {
  return {
    type: "ON_APPLY_CHANGES_TOGGLE",
    applyChanges: applyChanges
  };
};

export const onEvaluation = message => {
  return {
    type: "ON_EVALUATION",
    message
  };
};
