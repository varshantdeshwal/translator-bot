const applyChanges = (state = true, action) => {
  switch (action.type) {
    case "ON_APPLY_CHANGES_TOGGLE": {
      return action.applyChanges;
    }

    default:
      return state;
  }
};
export default applyChanges;
