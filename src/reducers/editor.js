const editorCode = (
  state = [
    "\r\n//Language codes for some languages: English en, Bengali bn,\r\n//Latin la, Hindi hi, Czech cs, Persian\tfa, Tamil ta,\r\n//Greek el, Telugu te, Gujarati gu, Thai th\r\n\r\n\r\nasync function respond(inputText) {\r\n    \r\n    //inputText is the input given by user.\r\n\t//translateFunction takes in 2 arguments\r\n    //text and target language code\r\n    \r\n\r\n    let result=await translateFunction(inputText,'hi');\r\n\r\n\t//result is the translated text\r\n\r\nreturn result;\r\n}"
  ],
  action
) => {
  switch (action.type) {
    case "ON_EDITOR_CODE_CHANGE": {
      let code = [...state];
      code[action.key] = action.code;
      return code;
    }
    case "ON_ADDING_EDITOR": {
      let code = [...state];
      code.push("//Write your code here");
      return code;
    }
    case "ON_CLOSING_EDITOR": {
      let code = [...state];
      code.splice(action.key, 1);
      return code;
    }
    default:
      return state;
  }
};
export default editorCode;
