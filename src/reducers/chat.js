const chat = (state = [], action) => {
  switch (action.type) {
    case "ON_EVALUATION": {
      let prevChat = [...state];
      if (
        action.message.type !== "user" &&
        action.message.message !== "..." &&
        prevChat.length !== 0
      ) {
        prevChat[prevChat.length - 1].message = action.message.message;
        return prevChat;
      } else {
        return [...state, action.message];
      }
    }

    default:
      return state;
  }
};
export default chat;
