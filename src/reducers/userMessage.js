const userMessage = (state = "", action) => {
  switch (action.type) {
    case "ON_USER_MESSAGE_CHANGE": {
      return action.userMessage;
    }

    default:
      return state;
  }
};
export default userMessage;
