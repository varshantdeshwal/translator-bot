import { combineReducers } from "redux";
import editorCode from "./editor";
import userMessage from "./userMessage";
import chat from "./chat";
import applyChanges from "./applyChanges";
import activeTab from "./activeTab";
export default combineReducers({
  editorCode,
  userMessage,
  chat,
  applyChanges,
  activeTab
});
