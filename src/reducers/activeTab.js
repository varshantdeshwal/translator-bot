const activeTab = (state = 0, action) => {
  switch (action.type) {
    case "ON_ACTIVE_TAB_CHANGED": {
      return action.key;
    }

    default:
      return state;
  }
};
export default activeTab;
